比特币现金（Bitcoin Cash）2022 升级

2022 年 5 月 15 日，比特币现金（Bitcoin Cash）将进行一项技术升级，旨在进一步扩展其虚拟机（VM）合约能力。这次升级将通过以下方式实现各种新的金融产品和服务：

1. 支持对更大数字的算术运算。扩展后的算术范围将允许合约以完整精确的方式管理非常大的余额，实现基于合约的金库（treasuries）并提高现有合约设计的效率。

2. 添加新操作，包括交易自我检查opcode。这些新opcode将使开发更安全的钱包、高效的定期支付等成为可能。

该升级已由公司、开发团队和独立开发人员组成的大型联盟进行讨论、开发和审查——包括所有现有的节点软件团队。截至 2021 年 11 月 15 日，相关审议已结束，无异议，此次升级将于 2022 年 5 月 15 日激活，并且不会造成网络分裂。

比特币现金节点 (BCHN) 软件将在其即将发布的 v24.0.0 版本中支持 2022 比特币现金升级。

升级时间表

- 2021 年 9 月 15 日 - 该升级已在公共测试网络上激活。
- 2021 年 11 月 15 日 - BCHN 对升级范围做出保证，并在11 月发布 v24.0.0版本
- 2022 年 2 月 15 日 - 预计所有生态系统软件都会发布支持该升级的稳定版本。
- 2022 年 5 月 15 日 - 在主网上激活该升级。

升级准备

对于比特币现金（Bitcoin Cash）用户，无需对本次升级进行准备； 在整个激活过程中可以安全地进行支付和接受付款。 现有的钱包软件无需升级即可继续运行。

建议矿工、交易所和其他节点运营商在 2022 年 5 月 15 日之前升级节点软件，以避免服务中断。 本次升级无需停机，升级后可继续正常运行。

技术细节

这次即将在2022 年 5 月 15 日进行的升级包括以下共识更改：

- [CHIP-2021-03：更大的脚本整数](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Bigger-Script-Integers.md)（[前往讨论](https://bitcoincashresearch.org/t/chip-2021-03-bigger-script-integers/39)）
- [CHIP-2021-02：交易自我检查opcode码](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md)（[前往讨论](https://bitcoincashresearch.org/t/chip-2021-02-add-native-introspection-opcodes/307)）

有关此次升级的更多 BCHN 相关信息，请访问

https://upgradespecs.bitcoincashnode.org/2022-05-15-upgrade/

——BCHN团队

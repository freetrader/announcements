Bitcoin Cash Node Technical Bulletin
--------------------------
Date: November 7, 2022

Publication: September 5, 2023

Author: matricz
 
---

## Summary

1. Introduction and rationale
2. Other Satoshi-based node software
3. Bitcoin Unlimited
4. libbitcoin
5. Knuth
6. flowee the hub
7. Bitcoin Verde
8. geth
9. bchd

## Introduction and rationale

This report investigates network architectures of software other than Bitcoin Cash Node, as exploratory research. A more indepth description of Bitcoin Cash Node's current network architecture is found in the [network architecture description](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-05-16_network_architecture_description_EN.md).

## Other Satoshi-based software

Bitcoin Core, Litecoin Core and Dogecoin Core all follow the same pattern described in the Bitcoin Cash Node network architecture report, inherited from the original satoshi software.

## Bitcoin Unlimited

Bitcoin Unlimited was forked from Core in 2016 has reworked its network architecture at the time of the Gigabit Testnet Initiative, as presented in this [hallmark talk from 2017](https://www.youtube.com/watch?v=5SJm2ep3X_M).

1. There is one message queue per connected node and also one global priority queue.

2. The startup thread will spin a number of message handler threads, up to the configured limit, with the default being the number of cores. Threads can be spun up or down during operation.

3. Each thread will grab a copy of `vNodes`, and all of the threads will iterate over all of the nodes.

4. For each node, a thread will `threadProcessMessages(pnode)` (which is notably a function and not a thread), and then SendMessages(pnode).

5. `threadProcessMessages(pnode)` mainly just does `g_signals.ProcessMessages(pnode)`.

6. `ProcessMessages` will start a loop on the current node's queue, until there is no more work (or the "send" queue for the node is saturated).

7. Inside the loop, if the there are any messages in the priority queue, it will work on that, rather than the current node's queue. Some other thread will take care of the current node sooner or later. Notably, only one thread will process priority messages at a time.

8. So now we have a msg, no matter how we got it (weather it's getdata, from normal queue or from priority queue).

9. We call the usual ProcessMessage on it (with several exceptions managed).

10. At the end of the loop there is some cleanup: if 2000 messages were processed in this loop, then bail with the follwoing comment "let someone else do something periodically".

11. if the message that was processed was a message from the priority queue (ie, the current thread has dropped the current node's queue to process the priority message) then restore processing the current node. 

12. Lasty, some messages from the priority queue can be delayed: when the send queue to the node that sent the priority message is full, then the thread doesn't process the priority message, but a normal message; at the end of processing the normal message, the priority message is added back to the priority queue.

This design, along with fine-grained locking, was specifically implemented to improve mempool admittance rate. It has been battle tested and bugs have been ironed out. It is still a little complex, expecially regarding the priority queue management. Mr Stone clarified that since this design was good enough for running 1GB blocks and relavitve addmintance rates, BU didn't delve into improving it further. 

## libbitcoin

libbitcoin is a "C++ Bitcoin toolkit library for asynchronous apps". The current version is `v3`, while `v4` is actively being developed.

It uses asynchronous patterns throughout, based on boost's `io_service`, and has a modular, [library-like structure](https://raw.githubusercontent.com/libbitcoin/libbitcoin-build/master/img/dependencies.png). Similarly to Verde, and to a bigger extent, callbacks are ubiquitous. 

In addition to the callback-driven architecture, several pub-sub points are implemented, so that applications that build upon the library can listen to relevant events. For example, `transaction_service` will listen for received transactions and then publish them via ZMQ. The pub-sub pattern makes sense to complement the callback-driven architecture, in the spirit of being a library and offering extension points to library users. 

In libbitcoin, almost eveything is handled by callbacks. Logical message exchanges are grouped into "protocols". A "protocol", in this context, is the logical sequence of request-response actions. For example an arriving INV will trigger a GET_DATA, which will result in incoming TX messages; this exchange, for example, is described in the `protocol_transaction_in.cpp`. 

On a low level, `libbitcoin-network` has `proxies` which own an (asynchronous) `socket`; the `proxies` are wrapped into a `channel`, which also maintains some state related to that connection (the `channel` is roughly equivalent to BCHN's `CNode`). There is one central `p2p` object which manages the `channels` via `sessions`, and dispatches and receives messages from them (the `p2p` object is roughly equivalent to BCHN's `CConnMan`). It is used from higher up in the module hierarchy. 

`libbitcoin-network` handles handshake, version negotiation and keep-alive pingpong messages, desribed in their own "protocols", assuming (correcly) that *any* application will use only "higher level" messages. In `libbitcoin`'s case, these "higher level" messages are handled by `libbitcoin-blockchain` and `libbitcoin-node`.

There is no concept of a message queue or explicit priority. All messages are handled via callbacks as soon as possible (until thead limit is reached, obviously) and dispatched to the correct submodule. This, combined with "protocols", naturally forms execution "strands". If prioritization needs to be achieved, a task (for example block validation) can be sumbitted to a thread pool with higher priority. 

## Knuth

Knuth is a BCH fullnode and development platform derived from libbitcoin v3. As far as the network architecture goes, it uses an unmodified version of libbitcoin, which makes it the only BCH fullnode software based on it.

## flowee the hub

flowee is a family of products whose "goal is to move the world towards a Bitcoin Cash economy." The `hub` is the fullnode component and it is derived from Core via BitcoinClassic. Since it forked from Core at a different time than ABC, several implementation details are different, but the general architecture (as of version `2022.04.1`) is the same as other Satoshi based clients.

There is a `ThreadSocketHandler` that handles low-level network traffic and a `ThreadMessageHandler` that handles peer messaging. 

Messages are handled by the the single `msghand` thread running the `ThreadMessageHandler` loop, with possible aid of worker threads for block processing. The increased performance of flowee comes from the lockless implementation of the unspent transaction databse.

While it is not integrated in the `hub` yet, the flowee project has a [Network Manager](https://flowee.org/docs/devel/network-manager/) component. It is currently to be used between the `hub` (ie. fullnode software) and user-facing applications. The Network Manger is built with the same boost's async `io_service` as libbitcoin. Its API is generally neat, and it can handle the basics of connections and keepalives, while exposing a "service" interface which can be used by implementing applications.

A "base service" is a piece of code that *receives* messages. For example, a function that handles the reception of TX messages could be a "base service'. A "service" (not "base") is a piece of code that *receives and responds* to messages. For example, a funtcion that receives an INV would check which TXs it is missing, and *respond* with an appropriate GETDATA. 
The "services/base services" need to extend one of the two appropriate classes and implement/override the `onIncomingMessage` method.

Alongside the `networkmanager` lib, flowee also implements a `p2p` lib, which is a specialization of the manager for the Bitcoin protocol. Currently, only part of the protocol is implemented, but there are plans to extend it to the full protocol and plug it into the `hub`.

## Bitcoin Verde

Bitcoin Verde (at version 2.2.0) is written in Java and uses MySQL/MariaDB as its database. It has several operating modes, some of which will just execute a command and exit (for example `SIGNATURE`) other will set up a long-running application (eg. `EXPLORER`, `STRATUM` or most notably `NODE`). `NODE` is the fullnode operating mode. 

1. If the application is started with `NODE`, a `NodeModule` will be created (in this context, "Node" means fullnode software).
2. The `NodeModule` is responsible for creating and initializing the application's subsystems, of which there are several (`_bitcoinNodeManager`, `_socketServer`, `_block/HeaderDownloader`, `_jsonRpcSocketServer`, `_blockchainBuilder`, `_transactionProcessor`, ...).
3. The `_socketServer`, of which there is only one, listens on a port/socket for new connections, and create new `BitcoinNode`s when they arrive (in this context, and from now on, a "Node" is the object responsabile for communicating with the network peer).
4. The `_socketServer` will pass the new `BitcoinNode` to the `_bitcoinNodeManager`, which finalizes its initialization.
5. The `BitcoinNode` can now send and receive network messages.
6. The `BitcoinNode` has an internal queue for sending messages, but it's active only to queue messages before the handshake is completed.
7. Once the peer is fully connected, the queue is not used; instead, threads that use the `BitcoinNode` will lock on it, waiting for another running thread to release the lock.
8. This effectively implements a message queue, because the threads that want to send are enqueued. Caveat: there is no guarantee on ordering, so it's more correctly a "message bag"; still, there is a guarantee to send messages in order, if a list of messages is sent in one call.
9. When a `Node` *receives* a message from a peer, it will call the appropriate handler from its handler list.

Regarding the inter-operation of the subsystems

1. There are four `ThreadPool`s (`_networkThreadPool` `_blockProcessingThreadPool`, `_generalThreadPool`, `_rpcThreadPool`): they are very big and are initialized with allowing for hundreds of concurrent threads. Threads are used lavishly troughout the app. 
2. Having several threadpools gives some amount of control on thread priority, eg. during block processing.
3. Each subsystem is initialized with one of the `TreadPool`s.
4. In an Object-Oriented fashion, the sybsystems are loosely coupled and communicate via message passing.
5. Messages exchanged are not tracked; rather a callback object (a `Runnable` exactly equivalent to C++'s lambda) will be attached to a message, to be executed at the end of the friendly subsystem's processing.
6. By virtue of closure, the callback will be executed in the context of where they are created, most often the calling subsystem. Often they will be ran in their own new threads, if it make sense to do so.
7. Given the sizes of the `ThreadPool`s, the application is massively concurrent. 

For example:

1. If the `_blockDownloader` wants to download a block, it will match `_bitcoinNodeManager`'s `Node`s for the given criteria (eg. the peer is a fullnode, is not pruned, it synched to the tip etc).
2. After a fitting peer is found, its `Node` is asked to send a GETBLOCK message in a new thread. A callback is created, that instructs the `Node` on what to do with the block once it arrives.
3. The current thread blocks and waits for the `Node`'s lock; once the lock is available again, one of the waiting threads (not necessarily FIFO) will lock it.
4. The `Node` will create the proper message, and update its internal request map, mapping the currently requested block to the callback it needs to execute when it's done.
5. The `Node` sends the data request
6. At a later time a `BLOCK` message will arrive. It will be matched to the correct handler
7. The `BLOCK` handler will match the newly arrived block with those requested in the internal request map; from there the correct callback will be executed in a new thread.

## geth
`geth` is fullnode client for Ethereum written in `go`. The language has the concept of goroutines and channels. `goroutines` are like "light threads" that share context with the calling thread (and sibling goroutines). `channels` are somewhat similar to queues, goroutines can use them to exchange data: a receiving routine "listens on the channel" and blocks until a message becomes available; a producing routine can write to the channel, but if the receiver is busy, it will block instead; if multiple producers are waiting, when the receving routine becomes available again, it will chose one of the producers *at random*.

`geth` has two operating modes, `fullnode` and `les` ("Light Ethereum Subprotocol"); fullnode can further run in regular full mode and in "archival" mode (which conserves more data and doesn't change much for this writeup).

1. In the fullnode mode, a global `server` object is created, that handles p2p networking
2. The `server` makes great use of channels and goroutines. goroutines run concurrently, but concurrency does not imply parallelism; but since go version 1.5, goroutines *can* run in parallel automagically. 
3. The `server` makes a thread in `listenLoop()` for accepting peer connections. The `server` maintains a map of connected `Peers` and creates a goroutine for each (`run()` in `p2p/peer.go`). 
4. Since goroutines are cheap, they are used liberally; for example a Peer will create one for receiving messages, one for ping, etc.
4. Each `Peer`'s routine will await network messages; if an incoming message is of "network-level" variety (handshakes, version negotiation, disconnects, ...) then it will be processed here; otherwise of it of "application-level" variety, it will be passed to a `protocol`
5. A `protocol` is a map that connects each message type to a handler function; for example the `eth66` protocol handles the `66` version of `eth` messages. 
6. There is no message queue towards the protocol, instead there is a channel; sending goroutines will block until their message is ready to be processed; if multiple messages are ready, one will be chosen *at random*
7. If the message is a transaction, it will indeed be enqueued for processing in the mempool; if it's a block, it will be processed right away.

The language makes it easy to use concurrency, and `geth`'s liberal use of it makes it a little difficult to reason about flow and performance. The idea of a `protocol` can be useful, especially around network upgrades, or to service diffrently peers with different capabilities.

---

The `les` mode is radically different and it's evident that it was implemented in a separate time with different architecture. It maintains a complex task queue, in which tasks are prioritized, and can be put on hold if higher priority tasks come in. Each peer is given a `buffer` (or an "allowance") with a `recharge rate`, and messages from the peers with most `buffer` are processed first. Peers consume their `buffer` proportionally to the computational complexity of the messages they send. This ensures that peers cannot DoS us easily. There are several task processors that read the task queue, but if a task processor is idle, a task will be passed onto it without passing through the queue (cool optimization).

## bchd

`bchd` is a Bitcoin Cash fullnode implementation written in go. Its github page states that it is a port of `btcd` for Bitcoin Cash, but it seems that the codebases have diverged after inception. 

Similarly to `geth` it uses goroutines and channels for concurrency. `server` is the central module, which coordinates the other modules and does some message processing. It has a goroutine for `ConnManager` which is responsible for the initial connection of new network `Peer`s, an array of `Peer`s (actually `ServerPeer`s) to receive network messages from and send to, manages a `TxMemPool` module, a `BlockChain` module, etc. 

Each `Peer` runs its own goroutines for managing its dataflow, which is split into 3 goroutines, `inHandler` for inbound messages, and `queueHandler` where the other modules post their outbound messages, and `outHandler` which actually sends them. The `Peer` is responsible for the initial handshake, version negotation, keep-alives etc, and generally the "network-level" messages. It will forward "application-level" messages to a `MessageListeners` struct, which will forward the message to an appropriate handler. In practice, all of the "application-level" messages are forwarded to the `server`, which processes them one at at time, intentionally. 

It is neat that each `Peer` handles its own messages concurrently (mainly the "network-level" messages) in its own goroutines, and that "network-level" messages are handled close to the network, concurrently (and in parallel) with the `server` doing some heavier computation like transaction or block processing.
